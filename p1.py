import numpy as np

input=[1,2,3,2.3]
weights=[[0.4,0.9,-0.5,1.0],
        [0.1,-0.9,0.3,1.0],
        [0.3,-0.2,-0.7,1.0]]
out=[2,4,0.5]
outputs=np.dot(weights,input)+out
print(outputs)
